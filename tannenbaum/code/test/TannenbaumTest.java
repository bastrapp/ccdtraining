package test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import src.Tannenbaum;

public class TannenbaumTest {

    private Tannenbaum tannenbaum;

    @Before
    public void setUp() {
        this.tannenbaum = new Tannenbaum();
    }

    @Test
    public void testZeichnen() {
        assertEquals(Arrays.asList("*", "X", "I"), this.tannenbaum.zeichnen(1));
        assertEquals(Arrays.asList(" *", " X", "XXX", " I"), this.tannenbaum.zeichnen(2));
        assertEquals(Arrays.asList("    *", "    X", "   XXX", "  XXXXX", " XXXXXXX", "XXXXXXXXX", "    I"),
                this.tannenbaum.zeichnen(5));
    }
}
