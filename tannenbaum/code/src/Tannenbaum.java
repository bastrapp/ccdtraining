package src;

import java.util.ArrayList;
import java.util.List;

public class Tannenbaum {

    public List<String> zeichnen(int hoehe) {
        List<String> baum = new ArrayList<>();

        baum.add(this.zeichne_stern(hoehe));
        baum.addAll(this.zeichne_baum_ebenen(hoehe));
        baum.add(this.zeichne_stamm(hoehe));

        return baum;
    }

    public String zeichne_stern(int hoehe) {
        String zeile = this.zeichne_spaces(hoehe, 0);
        return this.haenge_symbol_an(zeile, '*');
    }

    public List<String> zeichne_baum_ebenen(int hoehe) {

        List<String> baum = IntStream.range(0, hoehe).mapToObj(i -> this.zeichne_baum_ebene(hoehe, i)).collect(Collectors.toList());
        return baum;
    }

    public String zeichne_stamm(int hoehe) {
        String zeile = this.zeichne_spaces(hoehe, 0);
        return this.haenge_symbol_an(zeile, 'I');
    }

    public String zeichne_baum_ebene(int baumhoehe, int ebene) {

        String baumEbene = this.zeichne_spaces(baumhoehe, ebene);

        baumEbene += this.zeichne_symbole(ebene);

        return baumEbene;
    }

    public String zeichne_spaces(int baumhoehe, int ebene) {
        return new String(new char[baumhoehe - ebene - 1]).replace("\0", " ");
    }

    public String zeichne_symbole(int ebene) {
        return new String(new char[ebene * 2 + 1]).replace("\0", "X");
    }

    public String haenge_symbol_an(String text, char symbol) {
        return text + symbol;
    }
}
